﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;
using INTERFACESDAL;

using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace DAL
{
    public class UserDAL : IUserContainerDAL,IUserDAL 
    {
        //connection string in configuratie
        private string connString = "server=localhost;database=movieapp;user=root;password=;";

        MySqlConnection DbCon;

        //Adds a user to database
        public void AddUser(UserDTO user)
        {
            string query = "INSERT INTO `user`( `Name`, `Email`, `Username`, `Password`) " +
                    "VALUES (@name,@email,@username,@password)";

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@name", user.Name);
                    cmd.Parameters.AddWithValue("@email", user.Email);
                    cmd.Parameters.AddWithValue("@username", user.Username);
                    cmd.Parameters.AddWithValue("@password", user.Password);

                    cmd.ExecuteReader();

                    
                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }
                
            }
            
            
        }

        //Gets User from database
        public UserDTO GetUserByUserName(string username)
        {
            UserDTO userModel = new UserDTO();
            string query = "SELECT * FROM user WHERE Username = @username";

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    command.Parameters.AddWithValue("@username", username);


                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        userModel.Id = Convert.ToInt32(dataReader["Id"]);
                        userModel.Name = dataReader["Name"].ToString();
                        userModel.Email = dataReader["Email"].ToString();
                        userModel.Username = dataReader["Username"].ToString();
                        userModel.Password = dataReader["Password"].ToString();
                    }

                    return userModel;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }
                return userModel;
            }
 

           
        }

        public void AddReview(ReviewDTO reviewDto)
        {
            string query = "INSERT INTO `review`(`Title`, `Recommended`, `Description`, `Rating`, `MovieId`, `UserId`) " +
                           "VALUES (@Title,@Recommended,@Description,@Rating,@MovieId,@UserId)";

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand cmd = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    cmd.Parameters.AddWithValue("@Title", reviewDto.Title);
                    cmd.Parameters.AddWithValue("@Recommended", reviewDto.Recommended);
                    cmd.Parameters.AddWithValue("@Description", reviewDto.Description);
                    cmd.Parameters.AddWithValue("@Rating", reviewDto.Rating);
                    cmd.Parameters.AddWithValue("@MovieId", reviewDto.MovieId);
                    cmd.Parameters.AddWithValue("@UserId", reviewDto.UserId);

                    cmd.ExecuteReader();


                }
                catch (MySqlException ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }

            }
        }

        public List<ReviewDTO> GetAllReviewsFromMovie(int movieId)
        {
            ReviewDTO reviewDto = new ReviewDTO();
            List<ReviewDTO> reviewList = new List<ReviewDTO>();
            string query = "SELECT `id`, `Title`, `Recommended`, `Description`, `Rating`, `MovieId`, `UserId` FROM review WHERE `MovieId` = @movieId";

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    command.Parameters.AddWithValue("@movieId", movieId);


                    MySqlDataReader dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        reviewDto.Id = Convert.ToInt32(dataReader["Id"]);
                        reviewDto.Title = dataReader["Title"].ToString();
                        reviewDto.Recommended = Convert.ToBoolean(dataReader["Recommended"]);
                        reviewDto.Description = dataReader["Description"].ToString();
                        reviewDto.Rating = Convert.ToInt32(dataReader["Rating"]);
                        reviewDto.MovieId = Convert.ToInt32(dataReader["MovieId"]);
                        reviewDto.UserId = Convert.ToInt32(dataReader["UserId"]);

                        reviewList.Add(reviewDto);
                    }

                    return reviewList;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }
                return reviewList;
            }

        }
    }
}
