﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using INTERFACESDAL;
using INTERFACESDAL.STRUCTS;
using MySql.Data.MySqlClient;

namespace DAL
{
    public class WatchListDAL : IWatchListDAL
    {
        private string connString = "server=localhost;database=movieapp;user=root;password=;";

        MySqlConnection DbCon;

        public bool CheckIfMovieIsInWatchlist(UserDTO user, int movieId)
        {

            string query = "SELECT * FROM watchlist WHERE MovieName = @movieId AND UserId = @userId";

            using (MySqlConnection DbCon = new MySqlConnection(connString))
            {
                MySqlCommand command = new MySqlCommand(query, DbCon);
                try
                {
                    DbCon.Open();

                    command.Parameters.AddWithValue("@movieId", movieId);
                    command.Parameters.AddWithValue("@userId", user.Id);


                    MySqlDataReader dataReader = command.ExecuteReader();

                    if (dataReader.Read() != false)
                    {
                        
                    }

                    return true;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    DbCon.Close();
                }
                return true;
            }
        }
    }
}
