﻿using System;
using System.Collections.Generic;
using System.Text;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace INTERFACESMOVIEAPI
{
    public interface IMovieContainer
    {
        SearchContainer<SearchMovie> GetTrendingMovies();
        Movie GetMovieById(int movieId);
    }
}
