﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESVIEW;
using TMDbLib.Objects.TvShows;

namespace LOGIC
{
    public class Review : IReview
    {
      
        public string Title { get; set; }
        public bool Recommended { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public int MovieId { get; set; }
        public int UserId { get;set; }
    }
}
