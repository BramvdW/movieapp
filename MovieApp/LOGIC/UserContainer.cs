﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL;
using INTERFACESDAL.STRUCTS;
using FACTORYLOGIC;
using INTERFACESVIEW;

namespace LOGIC
{
    public class UserContainer : IUserContainerView
    {
        
        

        public bool AddUser(UserDTO user)
        {
            
            if (user.Name == null)
            {
                return false;
            } else if (user.Username == null)
            {
                return false;
            } else if (user.Email == null)
            {
                return false;
            }
            else if (user.Password == null)
            {
                return false;
            }
            else
            {
                UserFactory.CreateUserContainerDal().AddUser(user);
                return true;

            }
        }

        public IUserView GetUserByName(string name)
        {
            UserDTO userModel = UserFactory.CreateUserContainerDal().GetUserByUserName(name);

            IUserView userView = new User(userModel.Name, userModel.Id, userModel.Username,userModel.Email);

            return (userView);
        }
    }
}
