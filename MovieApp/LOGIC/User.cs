﻿using System;
using System.Collections.Generic;
using System.Text;
using FACTORYLOGIC;
using INTERFACESDAL;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;

namespace LOGIC
{
    public class User : IUserView
    {
        public User()
        {
        }

        public User(string name, int id, string username, string email)
        {
            this.Name = name;
            this.Id = id;
            this.Username = username;
            this.Email = email;
        }

        public string Name { get;}
        
        public int Id { get;}

        public string Username { get;}

        public string Email { get;}

        public bool AddReview(IReview review)
        {
            ReviewDTO reviewDto = new ReviewDTO();

            reviewDto.Title = review.Title;
            reviewDto.Description = review.Description;
            reviewDto.Recommended = review.Recommended;
            reviewDto.Rating = review.Rating;
            reviewDto.MovieId = review.MovieId;
            reviewDto.UserId = review.UserId;

            if (review.Title == null)
            {
                return false;
            }
            else
            {
                UserFactory.CreateUserDal().AddReview(reviewDto);
                return true;

            }
}

        public List<IReview> GetAllReviewsFromMovie(int movieId)
        {
            List<IReview> reviews = new List<IReview>();

            foreach (var item in UserFactory.CreateUserDal().GetAllReviewsFromMovie(movieId))
            {
                IReview iReview = new Review();

                iReview.Title = item.Title;
                iReview.Description = item.Description;
                iReview.Rating = item.Rating;
                iReview.Recommended = item.Recommended;


                reviews.Add(iReview);
            }

            return reviews;
        }

        public List<TMDbLib.Objects.General.Video> GetVideoList()
        {
            throw new NotImplementedException();
        }
    }

}
