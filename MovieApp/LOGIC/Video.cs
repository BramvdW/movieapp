﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LOGIC
{
    public class Video
    {
        public string Name;
        public string Description;
        public string Director;
        public string Storyline;



        public List<string> Genres;
        public int Rating;
    }
}
