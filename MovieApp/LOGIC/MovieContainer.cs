﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;
using FACTORYAPI;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;


namespace LOGIC
{
    public class MovieContainer : IMovieContainerView
    {
        MovieDTO mv = new MovieDTO();

        public SearchContainer<SearchMovie> GetAllTrendingMovies()//////
        {
            return MovieContainerFactoryApi.CreateMovieContainer().GetTrendingMovies();
        }

        public MovieDTO GetMovieById(int movieId)///////
        {
            TMDbLib.Objects.Movies.Movie movie = MovieContainerFactoryApi.CreateMovieContainer().GetMovieById(movieId);

            mv.Name = movie.Title;
            

            //foreach (Genre genre in movie.Genres)
            //{
            //    mv.genres.Add(genre.ToString());
            //}


            mv.Description = movie.Overview;
            mv.Id = movie.Id;

            return mv;
        }


    }
}
