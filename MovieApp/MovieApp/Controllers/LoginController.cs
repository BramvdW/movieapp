﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FACTORYVIEW;
using MODELS;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;
using LOGIC;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MovieApp.Controllers
{


    public class LoginController : Controller
    {
        public LoginController(AuthenticationService authenticationService)
        {
            AuthenticationService = authenticationService;
        }

        public AuthenticationService AuthenticationService { get; }

        //private ILoginController loginController;


        public IActionResult LoginPage()
        {
            return View(new UserModel());
        }

        [HttpPost]
        public IActionResult LoginPage(UserModel model)
        {
            

            if (AuthenticationService.LoginCheck(model.Username, model.Password))
            {
                IUserView userModel = UserFactoryView.CreateUserContainerLogic().GetUserByName(model.Username);

                //slaat de user op in sessions
                HttpContext.Session.SetInt32("UserId", userModel.Id);
                HttpContext.Session.SetString("Username", userModel.Username.ToString());
                HttpContext.Session.SetString("UserLvl", userModel.Email);

                ViewBag.TestSession = HttpContext.Session.GetString("Username");

                ViewBag.LoginData = "Login Succesfull";
            }
            else
            {
                ViewBag.LoginData = "Login Unsuccesfull";
            }

            return View();
        }
    }
}