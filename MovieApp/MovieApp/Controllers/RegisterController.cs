﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FACTORYVIEW;
using Microsoft.AspNetCore.Mvc;
using MODELS;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;
using LOGIC;

namespace MovieApp.Controllers
{
    public class RegisterController : Controller
    {
        

        public IActionResult NewAccount()
        {
            return View(new UserModel());
        }

        [HttpPost]
        public IActionResult NewAccount(UserModel model)
        {
            
            UserDTO user = new UserDTO();

            user.Name = model.Name;
            user.Email = model.Email;
            user.Username = model.Username;
            user.Password = model.Password;

            

            if (UserFactoryView.CreateUserContainerLogic().AddUser(user))
            {
                ViewBag.RegisterData = "Register Succesfull";
            }
            else
            {
                ViewBag.RegisterData = "Register Unsuccesfull";
            }

            return View();
        }
    }
}