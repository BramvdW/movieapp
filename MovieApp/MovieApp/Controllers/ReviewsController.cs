﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FACTORYVIEW;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;
using LOGIC;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MODELS;

namespace MovieApp.Controllers
{
    public class ReviewsController : Controller
    {
        public IActionResult ReviewPage(int movieId,string movieName)
        {
            ViewData["Id"] = movieId;
            ViewData["Name"] = movieName;




            return View();
        }

        [HttpPost]
        public IActionResult ReviewPage(ReviewModel reviewModel)
        {
            IReview iReview = new Review();

            iReview.Title = reviewModel.Title;
            iReview.Recommended = reviewModel.Recommended;
            iReview.Description = reviewModel.Description;
            iReview.Rating = reviewModel.Rating;
            iReview.MovieId = reviewModel.MovieId;
            iReview.UserId = Convert.ToInt32(HttpContext.Session.GetInt32("UserId")); 

            


            if (UserFactoryView.CreateUserView().AddReview(iReview))
            {
                ViewBag.RegisterData = "Review added";
            }
            else
            {
                ViewBag.RegisterData = "Adding the review was unsuccessful";
            }

            TempData["MovieId"] = iReview.MovieId;
            return RedirectToAction("MoviePage","Movie");
        }
    }
}