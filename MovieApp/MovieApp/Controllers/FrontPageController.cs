﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieApp.Models;
using LOGIC;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;


namespace MovieApp.Controllers
{
    public class FrontPageController : Controller
    {
        public IActionResult Index()
        {
            MovieContainer allTrendingMovies = new MovieContainer();
            SearchContainer<SearchMovie> movies = allTrendingMovies.GetAllTrendingMovies();

            List<SearchMovie> listAsString = movies.Results.ToList();



            ViewData["PopulairMovies"] = listAsString;

            return View(listAsString);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}