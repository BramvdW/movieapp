﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using FACTORYVIEW;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MovieApp.Controllers
{
    public class MovieController : Controller
    {
        public IActionResult MoviePage(int movieId)
        {
            MovieDTO movie = new MovieDTO();
            List<IReview> reviewList = new List<IReview>();

            if (movieId == 0)
            {
                movie = MovieContainerFactoryView.CreateMovieContainerView().GetMovieById(Convert.ToInt32(TempData["MovieId"]));
                reviewList = UserFactoryView.CreateUserView().GetAllReviewsFromMovie(Convert.ToInt32(TempData["MovieId"]));
            }
            else
            {

                movie = MovieContainerFactoryView.CreateMovieContainerView().GetMovieById(movieId);
                reviewList = UserFactoryView.CreateUserView().GetAllReviewsFromMovie(movieId);
            }



            ViewData["movieName"] = movie.Name;
            ViewData["movieDescription"] = movie.Description;
            ViewData["movieId"] = movie.Id;

            return View(reviewList);
        }

        [HttpPost]
        public IActionResult MoviePage()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("Username")))
            {
                return RedirectToAction("LoginPage", "Login");
            }

            string username = HttpContext.Session.GetString("Username");

            //if movie is not in watchlist add it to watchlist

            UserFactoryView.CreateUserContainerLogic().GetUserByName(username);

            if (true/*WatchlistFactoryView.CreateWatchListView().CheckIfMovieIsInWatchlist()*/)// movie is not in watchlist
            {
                //add to watchlist
            }



            return View();
        }
        
    }
}