﻿using System;
using System.Collections.Generic;
using System.Text;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;
using INTERFACESMOVIEAPI;

namespace MovieApiData
{
    public class IMDBDAL : IMovieContainer
    {
        string apiKeyExample = "https://api.themoviedb.org/3/trending/tv/day?api_key=5e2b5911d4523381b81485154f88c79a";
        
        TMDbClient client = new TMDbClient("5e2b5911d4523381b81485154f88c79a");

        public SearchContainer<SearchMovie> GetTrendingMovies()
        {
            SearchContainer<SearchMovie> movies = client.GetTrendingMoviesAsync(TMDbLib.Objects.Trending.TimeWindow.Week).Result;
            return movies;
        }


        public Movie GetMovieById(int movieId)
        {
            Movie movie = client.GetMovieAsync(movieId).Result;
            return movie;
        }
    }
}
