﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;

namespace INTERFACESDAL
{
    public interface IWatchListDAL
    {
        bool CheckIfMovieIsInWatchlist(UserDTO user, int movieId);
    }
}
