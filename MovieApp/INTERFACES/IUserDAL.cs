﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;

namespace INTERFACESDAL
{
    public interface IUserDAL
    {
        void AddReview(ReviewDTO reviewDto);
        List<ReviewDTO> GetAllReviewsFromMovie(int movieId);
    }
}
