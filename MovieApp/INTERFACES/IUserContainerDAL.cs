﻿using System;
using System.Collections.Generic;
using System.Text;

using INTERFACESDAL.STRUCTS;

namespace INTERFACESDAL
{
    public interface IUserContainerDAL
    {
        void AddUser(UserDTO user);
        UserDTO GetUserByUserName(string username);
    }
}
