﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTERFACESDAL.STRUCTS
{
    public struct ReviewDTO
    {
        public int Id;
        public string Title;
        public bool Recommended;
        public string Description;
        public int Rating;
        public int MovieId;
        public int UserId;

    }
}
