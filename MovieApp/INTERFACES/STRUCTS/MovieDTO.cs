﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTERFACESDAL.STRUCTS
{
    public struct MovieDTO
    {
        public int Id;
        public string Name;
        public string Description;
        public string Director;
        public string Storyline;

    }
}
