﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;
using TMDbLib.Objects.Movies;

namespace INTERFACESVIEW
{
    public interface IMovieContainerView
    {
        SearchContainer<SearchMovie> GetAllTrendingMovies();
        MovieDTO GetMovieById(int movieId);
    }
}
