﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;

namespace INTERFACESVIEW
{
    public interface IWatchListView
    {
        bool CheckIfMovieIsInWatchlist(UserDTO user, int movieId);
    }
}
