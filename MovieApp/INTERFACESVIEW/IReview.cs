﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTERFACESVIEW
{
    public interface IReview
    {
        string Title { get; set; }
        bool Recommended { get; set; }
        string Description { get; set; }
        int Rating { get; set; }
        int MovieId { get; set; }
        int UserId { get; set; }
    }
}
