﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;

namespace INTERFACESVIEW
{
    public interface IUserContainerView
    {
        bool AddUser(UserDTO user);

        IUserView GetUserByName(string name);
    }
}
