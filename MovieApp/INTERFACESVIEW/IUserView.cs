﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;
using TMDbLib.Objects.General;

namespace INTERFACESVIEW
{
    public interface IUserView
    {
        string Name { get;}
        int Id { get;}
        string Username { get;}
        string Email { get;}

        List<Video> GetVideoList();

        bool AddReview(IReview reviewDto);

        List<IReview> GetAllReviewsFromMovie(int movieId);
    }
}
