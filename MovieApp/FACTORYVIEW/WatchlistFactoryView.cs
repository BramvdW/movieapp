﻿using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESVIEW;
using LOGIC;

namespace FACTORYVIEW
{
    public static class WatchlistFactoryView
    {
        public static IWatchListView CreateWatchListView()
        {
            IWatchListView watchListView = new Watchlist();
            return watchListView;
        }
    }
}
