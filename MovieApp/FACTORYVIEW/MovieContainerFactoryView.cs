﻿using System;
using INTERFACESVIEW;
using LOGIC;
using MovieApiData;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Search;

namespace FACTORYVIEW
{
    public static class MovieContainerFactoryView
    {
       

        public static IMovieContainerView CreateMovieContainerView()
        {
            IMovieContainerView iMovieContainerView = new MovieContainer();
            return iMovieContainerView;
        }

        
    }
}
