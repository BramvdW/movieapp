﻿using System;
using INTERFACESDAL.STRUCTS;
using INTERFACESVIEW;
using LOGIC;

namespace FACTORYVIEW
{
    public static class UserFactoryView
    {
        public static IUserContainerView CreateUserContainerLogic()
        {
            IUserContainerView userContainerView = new UserContainer();
            return userContainerView;
        }

        public static IUserView CreateUserView()
        {
            IUserView userView = new User();
            return userView;
        }

    }
}
