﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MODELS
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
