﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MODELS
{
    public class ReviewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Recommended { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public int MovieId { get; set; }
        public int UserId { get; set; }
    }
}
