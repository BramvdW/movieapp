﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MODELS
{
    public class WatchListModel
    {
        public int Id { get; set; }
        public string MovieName { get; set; }
        public bool Watched { get; set; }
        public int UserId { get; set; }
    }
}
