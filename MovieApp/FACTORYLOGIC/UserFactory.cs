﻿using DAL;
using INTERFACESDAL;
using System;
using System.Collections.Generic;
using System.Text;
using INTERFACESDAL.STRUCTS;

namespace FACTORYLOGIC
{
    public static class UserFactory
    {
        public static IUserContainerDAL CreateUserContainerDal()
        {
           IUserContainerDAL userContainerDal = new UserDAL();
           return userContainerDal;
        }

        public static IUserDAL CreateUserDal()
        {
            IUserDAL userDal = new UserDAL();
            return userDal;
        }

    }
}
